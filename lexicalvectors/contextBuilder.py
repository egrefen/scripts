from __future__ import with_statement
import sys
import os


def contextBuilder(corpus, basis, normalize):
    with open(corpus) as f:
        for line in f:
            elems = line.strip().split()
            for i in range(len(elems)):
                    writeContextLine(elems, i, basis, normalize)


def writeContextLine(elems, i, basis, normalize):
    word = elems[i]
    if not word.isalnum():
        return
    if normalize:
        word = word.lower()
    context = getContext(elems, i, basis, normalize)
    context = [" ".join([word, x]) for x in context]
    if len(context) is not 0:
        print '\n'.join(context)


def getContext(elems, i, basis, normalize):
    if i == 0:
        context = elems[i + 1:1 + 5]
    else:
        context = elems[max(0, i - 5):i - 1] + elems[i + 1:i + 5]
    if normalize:
        context = [word.lower() for word in context]
    if basis is not None:
        context = [word for word in context if word in basis]
    return context


if __name__ == '__main__':
    if len(sys.argv) < 3:
        print "Usage: python", os.path.basename(sys.argv[0]), "corpus [basis] [normalize-flag]"
        sys.exit(0)

    corpus = sys.argv[1]

    if len(sys.argv) > 2:
        basis = [line.strip() for line in open(sys.argv[2])]
    else:
        basis = None

    normalize = len(sys.argv) > 3

    contextBuilder(corpus, basis, normalize)
