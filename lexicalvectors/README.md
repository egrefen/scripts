lexicalvectors
==============

Scripts for the construction of lexical vectors from the commandline.

basismaker.sh
=========

This script constructs a basis file corresponding to the top `size` words in a `corpus`, excluding words of length two or less (hard coded), and words in the `stoplist`.

**Usage:**

    $ basismaker.sh corpus stoplist size > basisfile

contextBuilder.py
=========

`TODO: ADD DESCRIPTION`

**Usage:**

    $ python contextBuilder.py corpus [basisfile] [normalize-flag] > contexts

vectorizer.sh
=========

`TODO: ADD DESCRIPTION`

**Usage:**

    $ vectorizer.sh contexts > vectors

rationorm.py
=========

`TODO: ADD DESCRIPTION`

**Usage:**

    $ python rationorm.py vectors > vectorsratio

pminorm.py
=========

`TODO: ADD DESCRIPTION`

**Usage:**

    $ python pminorm.py vectors > vectorsPMI
