from __future__ import with_statement, division
import sys
import os
import math


def applyratio(vectorFile):
    termfreq, contextfreq, total = getfreqs(vectorFile)

    for line in open(vectorFile):
        if len(line.strip()) is 0:
            pass
        parts = line.strip().split()
        term = parts[0]
        context = parts[1]
        count = int(parts[2])
        newcount = (count * total) / (termfreq[term] * contextfreq[context])
        print term, context, math.log(newcount)


def getfreqs(vectorFile):
    termfreq = {}
    contextfreq = {}
    total = 0
    for line in open(vectorFile):
        if len(line.strip()) is 0:
            pass
        term, context, count = tuple(line.strip().split())
        count = int(count)
        termfreq[term] = termfreq.get(term, 0) + count
        contextfreq[context] = contextfreq.get(context, 0) + count
        total += count
    return termfreq, contextfreq, total


if __name__ == '__main__':
    if len(sys.argv) < 2:
        print "Usage: python", os.path.basename(sys.argv[0]), "vectors"
        sys.exit(0)
    applyratio(sys.argv[1])
