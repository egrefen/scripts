import numpy
from scipy.sparse import lil_matrix
import cPickle as pickle

def words2IndexDict(wordFile):
    words = (line.strip() for line in open(wordFile))
    wordIndexDict = {word: index for index, word in enumerate(words)}
    return wordIndexDict

def vec2array(vectorFile, vocabIndexDict, basisIndexDict, existingArray = None):

    if existingArray is None:
        M = len(vocabIndexDict)
        N = len(basisIndexDict)
        termContextMatrix = numpy.zeros((M,N))
    else:
        M, N = existingArray.shape
        termContextMatrix = existingArray

    for line in open(vectorFile):
        if len(line) < 1:
            continue
        term, basisElement, countString = line.strip().split()
        termIndex = vocabIndexDict[term]
        basisIndex = basisIndexDict[basisElement]
        count = float(countString)
        termContextMatrix[termIndex, basisIndex] += count

    return lil_matrix(termContextMatrix)

def dumpIndexDict(indexDict, filename):
    with open(filename,'w') as f:
        for key in indexDict:
            f.write("{0} {1}\n".format(key, indexDict[key]))

def loadIndexDict(filename):
    indexDict = {}

    for line in open(filename):
        key, valuestring = line.strip().split()
        value = float(valuestring)
        indexDict[key] = value

    return indexDict

def main(argv):
    vectorFile = argv[1]
    vocabFile = argv[2]
    basisFile = argv[3]
    vocabIndexDict = words2IndexDict(vocabFile)
    basisIndexDict = words2IndexDict(basisFile)

    outputPkl = argv[4]
    vocabDictFile = argv[5]
    basisDictFile = argv[6]

    termContextMatrix = vec2array(vectorFile, vocabIndexDict, basisIndexDict)
    with open(outputPkl, 'wb') as pickleFile:
        pickle.dump(termContextMatrix, pickleFile, pickle.HIGHEST_PROTOCOL)
    dumpIndexDict(vocabIndexDict, vocabDictFile)
    dumpIndexDict(basisIndexDict, basisDictFile)

if __name__ == '__main__':
    import sys
    if len(sys.argv) < 7:
        print "Usage: ", sys.argv[0], "vectorFile vocabFile basisFile outputPkl vocabDictFile basisDictFile"
        sys.exit(1)
    main(sys.argv)