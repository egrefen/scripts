import numpy
from scipy.sparse import lil_matrix
import cPickle as pickle
from progressbar import ProgressBar, Percentage, Bar, ETA
import sys
import logging
logging.basicConfig(filename='binarycontexts2kronarray.log',level=logging.DEBUG)

HARDCODEDLINES = 4292028

def words2IndexDict(wordFile):
    words = (line.strip() for line in open(wordFile))
    wordIndexDict = {word: index for index, word in enumerate(words)}
    return wordIndexDict

def bicontext2array(verbContextFile, vocabIndexDict, verbIndexDict, lexicalVectors, existingArray = None):

    _, basisSize = lexicalVectors.shape

    if existingArray is None:
        M = len(verbIndexDict)
        N = basisSize**2
        verbContextMatrix = numpy.zeros((M,N))
    else:
        M, N = existingArray.shape
        verbContextMatrix = existingArray


    pbar = ProgressBar(widgets=[Percentage(), Bar(), ETA()], maxval=HARDCODEDLINES).start()
    linecount = 0
    for line in open(verbContextFile):
        linecount+=1
        if len(line) < 1:
            continue
        verb, subj, obj = line.strip().split()

        if verb not in verbIndexDict:
            pbar.update(linecount)
            continue

        verbIndex = verbIndexDict[verb]

        try:
            subjIndex = vocabIndexDict[subj]
            objIndex = vocabIndexDict[obj]
        except KeyError, e:
            logging.debug(str(e))
            # print >> sys.stderr, e
            pbar.update(linecount)
            continue

        subjVector = lexicalVectors[subjIndex,:]
        objVector = lexicalVectors[objIndex,:]
        verbContextMatrix[verbIndex, :] += numpy.kron(subjVector, objVector)
        pbar.update(linecount)

    pbar.finish()

    return lil_matrix(verbContextMatrix)

def dumpIndexDict(indexDict, filename):
    with open(filename,'w') as f:
        for key in indexDict:
            f.write("{0} {1}\n".format(key, indexDict[key]))

def loadIndexDict(filename):
    indexDict = {}

    for line in open(filename):
        key, valuestring = line.strip().split()
        value = float(valuestring)
        indexDict[key] = value

    return indexDict

def main(argv):
    verbContextFile = argv[1]
    vectorFile = argv[2]
    verbFile = argv[3]
    vocabDictFile = argv[4]
    verbIndexDict = words2IndexDict(verbFile)
    vocabIndexDict = loadIndexDict(vocabDictFile)
    outputPkl = argv[5]
    verbDictFile = argv[6]

    lexicalVectors = pickle.load(open(vectorFile,'rb')).toarray()

    verbContextMatrix = bicontext2array(verbContextFile, vocabIndexDict, verbIndexDict, lexicalVectors)

    with open(outputPkl, 'wb') as pickleFile:
        pickle.dump(verbContextMatrix, pickleFile, pickle.HIGHEST_PROTOCOL)

    dumpIndexDict(verbIndexDict, verbDictFile)

if __name__ == '__main__':
    import sys
    if len(sys.argv) < 7:
        print "Usage: ", sys.argv[0], "verbContextFile vectorFile verbFile vocabDictFile outputPkl verbDictFile"
        sys.exit(1)
    main(sys.argv)