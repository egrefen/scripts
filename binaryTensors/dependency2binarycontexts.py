#!/usr/bin/env python

def cleanTaggedWord(taggedWord):
    cleanword, _ = taggedWord.split('|', 1)
    return cleanword.lower()

def processchunk(chunk):
    chunkdict = {}
    for line in chunk:
        parts = line.split()
        if len(parts) <3:
            continue
        rel = parts[0]
        head = parts[1]
        tail = parts[2]
        headdict = chunkdict.setdefault(head,{})
        rellist = headdict.setdefault(rel,[])
        rellist.append(tail)
    for head in chunkdict:
        if "ncsubj" in chunkdict[head] and "dobj" in chunkdict[head]:
            for subj, obj in [(x, y) for x in chunkdict[head]["ncsubj"] for y in chunkdict[head]["dobj"]]:
                print cleanTaggedWord(head), cleanTaggedWord(subj), cleanTaggedWord(obj)


def main(argv):
    dependencyfile = argv[1]
    chunk = []

    for line in open(dependencyfile):
        line = line.strip()
        if len(line) > 0 and not line.startswith('<c>') and not line.startswith('#'):
            chunk.append(line)
        else:
            processchunk(chunk)
            chunk = []

    if len(chunk) > 0:
        processchunk(chunk)

if __name__ == '__main__':
    import sys
    if len(sys.argv) < 2:
        print "Usage:", sys.argv[0], "dependencyfile"
        sys.exit(1)
    main(sys.argv)
