#!/bin/bash

## Counts the number of unique words each type has in its type dictionary in a C&C parsed corpus (argument $1)

cat $1 | grep '<c>' | sed 's/^<c> //g' | tr ' ' '\n' | sed 's/|/ /g' | awk '$2 !~ /[^a-zA-Z]/ {print $2, $6}' | sort | uniq | awk '{print $2}' | sort | uniq -c | awk '{print $2, $1}' | sort -nr -k 2
