#!/bin/bash

# $1 Corpus
# $2 Stoplist
# $3 Basis size
# $4 Output Dir Name

mkdir $4;
./basismaker.sh $1 $2 $3 > $4/basisfile;
echo 'Made basis file at' $4/basisfile'.';
python ./contextBuilder.py $1 $4/basisfile yes > $4/contexts;
echo 'Contexts built.';
./vectorizer.sh $4/contexts > $4/vectors;
python ./rationorm.py $4/vectors > $4/vectorsratio;
echo 'Done!'
