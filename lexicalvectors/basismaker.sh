#!/bin/bash

# $1 Corpus
# $2 Stoplist
# $3 Basis size

cat $1 | tr ' ' '\n' | grep -v '[^a-zA-Z]' | awk 'length($1) > 2 {print}' | tr '[A-Z]' '[a-z]' | awk '{word[$0]++}; END{for (i in word) print i, word[i]}' | sort -k 2 -nr | awk '{print $1}' | grep -vwFf $2 | head -n $3
