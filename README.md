scripts
=======

This repository has some work in progress scripts that are either experimental or non-functional.

Use with caution...

lemmatizer
==========

Wrapper scripts for use with [John Carrol's lemmatizer](https://github.com/knowitall/morpha).

lexicalvectors
==============

Scripts for generating lexical semantic vectors using an n-word window method, and applying selected weighting schemes.
