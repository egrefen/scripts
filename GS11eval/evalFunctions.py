from __future__ import division
import numpy
import math
from scipy.stats import spearmanr
import cPickle as pickle

class VectorDict(object):

    def __init__(self, vectors, indexDict):
        self.vectors = vectors
        self.indexDict = indexDict

    def __getitem__(self, key):
        index = self.indexDict[key]
        return self.vectors[index, :]


def loadVectorDict(vectorFile, indexDictFile):
    vectors = pickle.load(open(vectorFile,'rb')).toarray()
    indexDict = loadIndexDict(indexDictFile)
    return VectorDict(vectors, indexDict)


def loadProbDict(probFile):
    lines = [line.strip() for line in open(probFile) if len(line.strip())>0]
    probDict = {}
    for line in lines[1:]:
        parts = line.split()
        sentence = " ".join(parts[0:3])
        logprob = float(parts[3])
        probDict[sentence] = logprob
    return probDict


def loadIndexDict(filename):
    indexDict = {}

    for line in open(filename):
        key, valuestring = line.strip().split()
        value = float(valuestring)
        indexDict[key] = value

    return indexDict


def loadDataset(datasetFile):
    lines = [line.strip() for line in open(datasetFile)]
    return lines[1:]


def cosine(v1, v2):
    lv1 = math.sqrt(numpy.dot(v1,v1))
    lv2 = math.sqrt(numpy.dot(v2,v2))
    dp = numpy.dot(v1, v2)
    cos = dp/(lv1*lv2)
    return cos


def iterateDataset(lines, builder, compare=cosine):
    goldScores = []
    modelScores = []
    for line in lines:
        participant, verb, subj, obj, landmark, goldscore, hilo = line.split()
        s1 = builder(subj, verb, obj)
        s2 = builder(subj, landmark, obj)
        modelscore = compare(s1, s2)
        goldScores.append(float(goldscore))
        modelScores.append(float(modelscore))
    return goldScores, modelScores


def getRho(goldScores,modelScores):
    return spearmanr(goldScores,modelScores)


def getInterannotatorAgreement(lines):
    annotatorDict = {}
    for line in lines:
        participant, verb, subj, obj, landmark, goldscore, hilo = line.split()
        pdict = annotatorDict.setdefault(participant, {})
        sID = ";;".join([verb, subj, obj, landmark])
        pdict[sID] = float(goldscore)
    arrayindices = {y:x for x,y in enumerate(annotatorDict[annotatorDict.keys()[0]].keys())}
    arraylength = len(arrayindices)
    pArrayDict = {}
    for annotator in annotatorDict:
        pArray = pArrayDict.setdefault(annotator,[None]*arraylength)
        pdict = annotatorDict[annotator]
        for sID in pdict:
            index = arrayindices[sID]
            pArray[index] = pdict[sID]
    annotators = pArrayDict.keys()

    apairs = []
    for a1 in annotators:
        for a2 in annotatorDict:
            if a1 != a2 and (a2,a1) not in apairs:
                apairs.append((a1,a2))

    agreements = []
    significances = []
    for a1, a2 in apairs:
        rho, p = getRho(pArrayDict[a1], pArrayDict[a2])
        agreements.append(rho)
        significances.append(p)

    avgAgreement = sum(agreements)/len(agreements)
    avgSignificance = sum(significances)/len(significances)
    print "UpperBound | rho", avgAgreement, "| p", avgSignificance


def getVerbOnly(lines, wordVectorDict):
    builder = lambda subj, verb, obj: wordVectorDict[verb]
    goldScores, modelScores = iterateDataset(lines, builder)
    rho, p = getRho(goldScores,modelScores)
    print "Verb Only | rho", rho, "| p", p


def getAdditive(lines, wordVectorDict):
    builder = lambda subj, verb, obj: wordVectorDict[subj] + wordVectorDict[verb] + wordVectorDict[obj]
    goldScores, modelScores = iterateDataset(lines, builder)
    rho, p = getRho(goldScores,modelScores)
    print "Additive | rho", rho, "| p", p


def getMultiplicative(lines, wordVectorDict):
    builder = lambda subj, verb, obj: wordVectorDict[subj] * wordVectorDict[verb] * wordVectorDict[obj]
    goldScores, modelScores = iterateDataset(lines, builder)
    rho, p = getRho(goldScores,modelScores)
    print "Multiplicative | rho", rho, "| p", p


def getKron(lines, wordVectorDict):
    builder = lambda subj, verb, obj:  numpy.kron(wordVectorDict[verb],wordVectorDict[verb]) * numpy.kron(wordVectorDict[subj],wordVectorDict[obj])
    goldScores, modelScores = iterateDataset(lines, builder)
    rho, p = getRho(goldScores,modelScores)
    print "Kron | rho", rho, "| p", p


def getCategorical(lines, wordVectorDict, verbVectorDict):
    builder = lambda subj, verb, obj:  verbVectorDict[verb] * numpy.kron(wordVectorDict[subj],wordVectorDict[obj])
    goldScores, modelScores = iterateDataset(lines, builder)
    rho, p = getRho(goldScores,modelScores)
    print "Categorical | rho", rho, "| p", p

def getBiProb(lines, biProbDict):
    builder = lambda subj, verb, obj: biProbDict[" ".join([subj, verb, obj])]
    compare = lambda s1, s2: s1+s2
    goldScores, modelScores = iterateDataset(lines, builder, compare)
    rho, p = getRho(goldScores,modelScores)
    print "BigramProb | rho", rho, "| p", p

def getTriProb(lines, triProbDict):
    builder = lambda subj, verb, obj: triProbDict[" ".join([subj, verb, obj])]
    compare = lambda s1, s2: s1+s2
    goldScores, modelScores = iterateDataset(lines, builder, compare)
    rho, p = getRho(goldScores,modelScores)
    print "TrigramProb | rho", rho, "| p", p

if __name__ == '__main__':
    import os.path as path
    import sys

    if len(sys.argv) < 3:
        datasetPath = "/home/edwgre/GS13data.txt"
        dataroot = "/home/edwgre/CLArticle2013experiments/BNCvectors/"
    else:
        datasetPath = sys.argv[1]
        dataroot = sys.argv[2]

    wordVectorPath =    path.join(dataroot,"vectorsratio.pkl")
    wordIndexDictPath = path.join(dataroot,"vocabDict")

    verbVectorPath =    path.join(dataroot,"verbs.pkl")
    verbIndexDictPath = path.join(dataroot,"verbDict")

    biProbdictPath = path.join(dataroot,"biProbs")
    triProbdictPath = path.join(dataroot,"triProbs")

    biProbDict = loadProbDict(biProbdictPath)
    triProbDict = loadProbDict(triProbdictPath)

    # verbVectorPath =    path.join(dataroot,"verbsVanilla.pkl")
    # verbIndexDictPath = path.join(dataroot,"verbVanillaDict")

    lines = loadDataset(datasetPath)
    # getInterannotatorAgreement(lines)

    # wordVectorDict = loadVectorDict(wordVectorPath, wordIndexDictPath)
    # verbVectorDict = loadVectorDict(verbVectorPath, verbIndexDictPath)

    # getVerbOnly(lines, wordVectorDict)
    # getAdditive(lines, wordVectorDict)
    # getMultiplicative(lines, wordVectorDict)
    # getKron(lines, wordVectorDict)
    # getCategorical(lines, wordVectorDict, verbVectorDict)
    getBiProb(lines, biProbDict)
    getTriProb(lines, triProbDict)
